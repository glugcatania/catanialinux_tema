<?php
if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['message']) || !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
  {
    echo "Attenzione! Valorizzare tutti i campi.";
    return false;
  }

$name = strip_tags(htmlspecialchars($_POST['name']));
$email_address = strip_tags(htmlspecialchars($_POST['email']));
$message = strip_tags(htmlspecialchars($_POST['message']));
$policy = strip_tags(htmlspecialchars($_POST['policy']));

$to = "info@catania.linux.it";
$from = "admin@catania.linux.it";
$subject = "Email ricevuta da " . $email_address . " proveniente dal form contatti del Sito web";
$html_body = "Hai ricevuto un nuovo messaggio dal form di contatto di catania.linux.it" . "<br><br>" . "<strong>Di seguito i dettagli:</strong>" . "<br><br>" . "<strong>Cognome e Nome:</strong> " . $name . "<br><br>" . "<strong>Email:</strong> " . $email_address . "<br><br>" . "<strong>Policy:</strong> " . $policy . "<br><br>" . "<strong>Messaggio:</strong>" . "<br>" . $message;

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
$headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $email_address . "\r\n";

mail($to, $subject, $html_body, $headers);
return true;
?>
