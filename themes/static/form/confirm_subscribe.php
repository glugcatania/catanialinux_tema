<?php
session_start();
include "fz_accessi_glugct.php";

if (empty($_SESSION['email']) || empty($_SESSION['token']) || !filter_var($_SESSION['email'],FILTER_VALIDATE_EMAIL))
  {
    $response = array('status' => false, 'message' => "Attenzione! Valorizzare tutti i campi.");
    echo json_encode($response);
    return false;
  }

$domain = "catania.linux.it";

$pp_hostname = "www.paypal.com"; // Real
//$pp_hostname = "www.sandbox.paypal.com"; // Test

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-synch';

$tx_token = $_POST['tx'];
$auth_token = ""; // Real
//$auth_token = ""; // Test
$req .= "&tx=$tx_token&at=$auth_token";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://$pp_hostname/cgi-bin/webscr");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
//set cacert.pem verisign certificate path in curl using 'CURLOPT_CAINFO' field here,
//if your server does not bundled with default verisign certificates.
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $pp_hostname"));
$res = curl_exec($ch);
curl_close($ch);

if ($res)
  {
    // parse the data
    $lines = explode("\n", trim($res));
    $keyarray = array();
    if (strcmp ($lines[0], "SUCCESS") == 0)
      {
        for ($i = 1; $i < count($lines); $i++)
          {
            $temp = explode("=", $lines[$i],2);
            $keyarray[urldecode($temp[0])] = urldecode($temp[1]);
          }

        // Ricavo i nome, cognome del compratore
        $firstname = $keyarray['first_name'];
        $lastname = $keyarray['last_name'];

        // Mi connetto adl db
        $link = connettidb();

        // Genero ed eseguo la query
        $query = "UPDATE subscribe SET stato = 1, importo = " . $_SESSION['importo'] . " WHERE stato = 0 AND email = '" . $_SESSION['email'] . "' AND token = '" . $_SESSION['token'] . "'";
        mysqli_query($link, $query) or die(mysqli_error($link));

        if (mysqli_affected_rows($link) == 1)
          {
            // Send confirm Email
            $to = "admin@catania.linux.it";
            $from = "info@catania.linux.it";
            $subject = "[" . $domain . "]" . " Notifica di iscrizione al sito";
            $html_body = "<a href='mailto:" . $_SESSION['email'] . "'>" . $_SESSION['email'] . "</a> è stato iscritto con successo al sito " . $domain;

            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $to . "\r\n";

            disconnettidb($link);
            $_SESSION['email'] = "";
            $_SESSION['token'] = "";
            $_SESSION['importo'] = "";
            session_destroy();

            if (mail($to, $subject, $html_body, $headers))
              {
                $response = array('status' => true, 'message' => "Grazie " . $firstname . " " . $lastname . ". La transazione è stata eseguita finalizzando la tua iscrizione.");
                echo json_encode($response);
                return true;
              }
          }
        else
          {
            disconnettidb($link);
            $_SESSION['email'] = "";
            $_SESSION['token'] = "";
            $_SESSION['importo'] = "";
            session_destroy();

            $response = array('status' => false, 'message' => "Iscrizione rifiutata. Verificare i valori digitati in fase di iscrizione.");
            echo json_encode($response);
            return false;
          }
      }
    else if (strcmp ($lines[0], "FAIL") == 0)
      {
        // log for manual investigation
      }
  }
else
  {
    // HTTP Error
  }
?>
