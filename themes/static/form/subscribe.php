<?php
include "fz_accessi_glugct.php";

if (empty($_POST['cognome']) || empty($_POST['nome']) || empty($_POST['data_nascita']) || empty($_POST['codfisc']) || empty($_POST['indirizzo']) || empty($_POST['cap']) || empty($_POST['prov']) || empty($_POST['citta']) || empty($_POST['email']) || !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
  {
    $response = array('status' => false, 'message' => "Attenzione! Valorizzare tutti i campi.");
    echo json_encode($response);
    return false;
  }

// Clean data
$socio = clr($_POST['socio']);
$cognome = clr(ucfirst($_POST['cognome']));
$nome = clr(ucfirst($_POST['nome']));
$data_nascita = clr($_POST['data_nascita']);  // attiva dal js in formato yyyy-mm-dd
$codfisc = clr(strtoupper($_POST['codfisc']));
$indirizzo = clr($_POST['indirizzo']);
$cap = clr($_POST['cap']);
$prov = clr(strtoupper($_POST['prov']));
$citta = clr(ucfirst($_POST['citta']));
$email = clr(strtolower($_POST['email']));
$data_iscrizione = date("Y-m-d");
$privacy = $_POST['policy'];
$stato = 0;
$token = md5(rand(0,1000)); // Genero un token di 32 caratteri
$importo = 0.00;
$domain = "catania.linux.it";
$url = "<a href='https://www.catania.linux.it/themes/form/store.php?email=".$email."&token=".$token."&socio=".$socio."'>https://www.catania.linux.it/themes/form/store.php?email=".$email."&token=".$token."&socio=".$socio."</a>";

// Check field format
if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $data_nascita))
  {
    $response = array('status' => false, 'message' => "Il campo Data di nascita non è valido.");
    echo json_encode($response);
    return false;
  }

if (!preg_match("/^[a-z]{6}[0-9]{2}[a-z][0-9]{2}[a-z][0-9]{3}[a-z]$/i", $codfisc))
  {
    $response = array('status' => false, 'message' => "Il campo Codice fiscale non è valido.");
    echo json_encode($response);
    return false;
  }

if (!preg_match("/^[0-9-]+$/", $cap))
  {
    $response = array('status' => false, 'message' => "Il campo CAP non è valido.");
    echo json_encode($response);
    return false;
  }

$link = connettidb();

$query1 = "SELECT * FROM subscribe WHERE email = '" . $email . "'";
$result = mysqli_query($link, $query1) or die(mysqli_error($link));

if (mysqli_num_rows($result) > 0)
  {
    $response = array('status' => false, 'message' => "Indirizzo email già esistente sui nostri archivi.");
    echo json_encode($response);
    return false;
  }
else
  {
    // Send confirm Email
    $from = "info@catania.linux.it";
    $subject = "[" . $domain . "]" . " Conferma l'indirizzo E-Mail";
    $html_body = "Ciao da " . $domain . "!" . "<br><br>" . "Per confermare, clicca qui " . $url . "<br><br>" . "Grazie da " . $domain . "!" . "<br>" . $domain . "!";

    // To send HTML mail, the Content-type header must be set
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $email . "\r\n";

    if (mail($email, $subject, $html_body, $headers))
      {
        // Insert record
        $query2 = "INSERT INTO subscribe (id_sub, socio, cognome, nome, data_nascita, codfisc, indirizzo, cap, prov, citta, email, data_iscrizione, privacy, stato, token, importo) VALUES (NULL, '" . $socio . "', '" . $cognome . "', '" . $nome . "', '" . $data_nascita . "', '" . $codfisc . "', '" . $indirizzo . "', '" . $cap . "', '" . $prov . "', '" . $citta . "', '" . $email . "', '" . $data_iscrizione . "', '" . $privacy . "', '" . $stato . "', '" . $token . "', '" . $importo . "')";
        mysqli_query($link, $query2) or die(mysqli_error($link));

        $response = array('status' => true, 'message' => "La tua iscrizione è stata registrata con successo. Riceverai una email per confermare l'iscrizione");
        echo json_encode($response);
        return true;
      }
  }

disconnettidb($link);
?>
