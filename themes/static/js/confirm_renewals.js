$(function(){
  // Get params value from url
  var url_string = window.location.search;
  var url_params = new URLSearchParams(url_string);
  var tx = url_params.get('tx');

  $(document).ready(function(){
    $.ajax({
      url: "themes/form/confirm_renewals.php",
      dataType:'JSON',
      type: "POST",
      data: {
      tx: tx
      },
      cache: false,
      success: function(response) {

      if (response.status == true)
        {
          // Success message
          $('#success').html("<div class='alert alert-success'>");
          $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
          $('#success > .alert-success').append($("<strong>").text(response.message));
          $('#success > .alert-success').append('</div>');
        }
      else
        {
          // Error message
          $('#success').html("<div class='alert alert-danger'>");
          $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
          $('#success > .alert-danger').append($("<strong>").text(response.message));
          $('#success > .alert-danger').append('</div>');
        }
      },
    });
  });
});
