function initPayPalButton() {
  paypal.Buttons({
    style: {
      shape: 'rect',
      color: 'gold',
      layout: 'vertical',
      label: 'paypal',
    },

    createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{"description":"Socio Ordinario","amount":{"currency_code":"EUR","value":10}}]
      });
    },

    onApprove: function(data, actions) {
      return actions.order.capture().then(function(orderData) {

        // Full available details
        console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));

        // Redirect conferma-iscrizione.html width transactionID GET
        var transaction = orderData.purchase_units[0].payments.captures[0];
        actions.redirect('https://www.catania.linux.it/conferma-iscrizione.html?tx=' + transaction.id);
      });
    },

    onError: function(err) {
      console.log(err);
    }
  }).render('#paypal-button-container');
}
