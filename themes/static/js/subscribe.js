$(function() {

    $("#subscribeForm input,#subscribeForm select").jqBootstrapValidation({
      preventSubmit: true,
      submitError: function($form, event, errors) {
        // additional error messages or events
      },
      submitSuccess: function($form, event) {
        event.preventDefault(); // prevent default submit behaviour
        // get values from FORM
        var socio = $("select#socio").val();
        var cognome = $("input#cognome").val();
        var nome = $("input#nome").val();
        var data_nascita = $("input#data_nascita").val();
        var codfisc = $("input#codfisc").val();
        var indirizzo = $("input#indirizzo").val();
        var cap = $("input#cap").val();
        var prov = $("input#prov").val();
        var citta = $("input#citta").val();
        var email = $("input#email").val();

        var policy = 0;
        if ($("input#policy").is(":checked"))
          {
            policy = 1;
          }

        $this = $("#sendSubscriptionButton");
        $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages
        $.ajax({
          url: "themes/form/subscribe.php",
          dataType:'JSON',
          type: "POST",
          data: {
            socio: socio,
            cognome: cognome,
            nome: nome,
            data_nascita: data_nascita,
            codfisc: codfisc,
            indirizzo: indirizzo,
            cap: cap,
            prov: prov,
            citta: citta,
            email: email,
            policy: policy
          },
          cache: false,
          success: function(response) {

            if (response.status == true)
              {
                // Success message
                $('#success').html("<div class='alert alert-success'>");
                $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                $('#success > .alert-success').append($("<strong>").text(response.message));
                $('#success > .alert-success').append('</div>');
                //clear all fields
                $('#subscribeForm').trigger("reset");
              }
            else
              {
                // Error message
                $('#success').html("<div class='alert alert-danger'>");
                $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                $('#success > .alert-danger').append($("<strong>").text(response.message));
                $('#success > .alert-danger').append('</div>');
              }
          },
          complete: function() {
            setTimeout(function() {
              $this.prop("disabled", false); // Re-enable submit button when AJAX call is complete
            }, 1000);
          }
        });
      },
      filter: function() {
        return $(this).is(":visible");
      },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
      e.preventDefault();
      $(this).tab("show");
    });
  });

  /*When clicking on Full hide fail/success boxes */
  $('#cognome').focus(function() {
    $('#success').html('');
  });
